# Contributing to Metalab documentations: guide

Thank you for considering contributing to our documentation !

Here are a few guidelines to help you get started.

## Tooling

All of our software runs on Ubuntu.

All of our documentation websites are made using [Sphinx](https://www.sphinx-doc.org) with reStructuredText and the [Furo theme](https://pradyunsg.me/furo/).

## Templates for the websites

The templates used for the websites are available at [sat-metalab/documentations/template-website](https://gitlab.com/sat-metalab/documentations/template-website).

## Current status of documentation

Currently, we focus on improving the onboarding documentation for projects Splash, SATIE, shmdata and LivePose. That means improving the installation instructions, writing overviews of the projects, and getting started tutorials.

We are currently writing the original version in English and translating to French.

We have a few guidelines to do both, but there is *no styleguide at this moment*.

Some of our inspiration for documentation comes from the [Python docs project](https://github.com/python/docs-community) and its [French translation](https://github.com/python/python-docs-fr).

All the documentations websites main repos are on [sat-metalab/documentations](https://www.gitlab.com/sat-metalab/documentations).

## How to contribute to one of the project documentation

For now (Aug 2022), we suggest one of the following ways:

- if you find a bug in the docs, or want to suggest a tiny change: file an *issue on Gitlab* on the corresponding [documentations](https://www.gitlab.com/sat-metalab/documentations) project
- if you wish to start working on adding something new to the documentation, like a tutorial or a guide: start a [discussion on Discourse](https://discourse.sat.qc.ca)
- not sure where to start ? chat with us on [Matrix](https://matrix.to/#/#sat_metalab:matrix.org)

## Current contribution process

**This is a work in progress.**

- Find something you wish to document: maybe it is something new! Maybe it is an improvment on existing documentation! Maybe it's a tiny but needed docs update!
- Open an issue or start a discussion on Discourse
- If you have an existing rough draft, feel welcome to share it - you can write in Markdown for the draft
- After your documentation project gets a first approval from the Metalab team, consider starting or improving the first draft and open an MR to share it
- Once the first draft is completed, there will be an editing step where someone from the Metalab team will review your documentation
- When the final version is reached, the MR is merged and the documentation is published.


## Contributing to this guide

We are working on improving the contribution process. You can make suggestion on how to improve this README on [Discourse](https://discourse.sat.qc.ca/t/facilitation-des-contributions-a-la-documentation/34) or by opening an issue in this repo.
