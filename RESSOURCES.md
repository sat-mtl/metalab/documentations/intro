# Learning how to doc

Resources to learn how to do technical documentation - as a developer, as a technical writer, as an editor or as a translator - with a focus on software documentation.

[[_TOC_]]

## Internal links

Some Metalab-specific instructions that might be helpful to you.

### Templates

#### Website

We use Sphinx. You can [learn how to use the template for our websites](https://gitlab.com/sat-metalab/documentations/template-website) and you can also [learn Sphinx basics in this self-directed workshop](https://gitlab.com/sat-metalab/workshops/sphinx-workshop).

### Documentation

Here are some [content templates](https://gitlab.com/sat-metalab/documentations/templates/-/tree/main/doc_model_0).

## External links

I started tech writing a few years now - here are the main resources I have used to help me along the way. Feel welcome to include yours by opening an issue on this repo.

### Finding communities

#### Write the Docs

If I were to start again, I would [join the Write the Docs community as soon as possible](https://www.writethedocs.org/). I learned many things through their [list of past articles, presentations and blogposts](https://www.writethedocs.org/about/learning-resources/) about documentation, was able to meet like-minded folks from around the world [on their Slack](https://www.writethedocs.org/slack/) and [networked during their conferences](https://www.writethedocs.org/conf/).

#### Python docs - French translation

I first started working on documentation as a contributor to the [French translation project for the official Python documentation](https://github.com/python/python-docs-fr).

I learned many things by contributing to open source. It made me a better writer and a better programmer.

#### Your local dev/tech/programming meetup

If you work on software documentation, it make sense to network with other people sharing this interest! Maybe consider giving a talk about documentation?

### Information architecture

#### Diátaxis

We usually try to follow the [Diátaxis framework](https://diataxis.fr/) for structuring our documentation.

#### How to make sense of any mess

Sometimes we have to admit that [we created a mess](http://www.howtomakesenseofanymess.com/) but we are figuring things out.

#### Every page is page one

As you write websites for documentation, you soon realize how everyone will not be arriving on your website from the same starting point, so [every page is page one](https://everypageispageone.com/).

### French resources

If you are going to be transating from/to French, or if you are writing in French, maybe you will find these useful!

#### Lexiques et vocabulaires

The [Office québécois de la langue française maintains many lists of words and concepts related to IT and computers](https://www.oqlf.gouv.qc.ca/ressources/bibliotheque/dictionnaires/index_lexvoc.html), like this one [about developing software](https://www.oqlf.gouv.qc.ca/ressources/bibliotheque/dictionnaires/vocabulaire-edition-logiciels.aspx).

#### Rédaction technique - blogue

This [blog about technical writing](https://redaction-technique.org/index.html) will help you get started documenting.

#### INF 229 - Rédaction technique en informatique (Université de Sherbrooke) - Luc Lavoie

This class about technical writing has [wonderful class notes](http://llavoie.espaceweb.usherbrooke.ca/llavoie/enseignement/INF229/) that covers many topics like using the active voice in French.

### Translation

#### English to/from French

Besides the previously mentioned resources, another one comes from the Governement of Canada and is called [TERMIUM Plus®](https://www.btb.termiumplus.gc.ca/).

### Tooling

#### Sphinx

The [getting started guide to Sphinx](https://www.sphinx-doc.org/en/master/tutorial/getting-started.html) was recently reviewed and improved!
















